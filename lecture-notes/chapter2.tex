\chapter{Sets}
In this chapter we look at some preliminary material on sets. Many books and online resources
consider these topics but of course there are slight variations in notation.
\section{Sets}
A set is a collection of objects (elements). Let $a$ be an element and let $A$ and $B$ be sets. We write $a \in B$ if $a$ is one of the elements in the collection $B$ and we write $a \notin B$ if it is not. We write $A \subseteq B$ if every element in the collection $A$ is also in the collection $B$ and we write $A\nsubseteq B$ if this is not the case. If $A\subseteq B$ we say $A$ is a subset of $B$.

\begin{exa}
	The following are some of the useful examples of sets in this course;
	\begin{enumerate}
		\item[(1)] $\mathbb{N}=\{0,1,2,3,...\}$ is the set of natural numbers.
		\item[(2)] $\mathbb{Z}=\{...,-3,-2,-1,0,1,2,3,...\}$ is the set of all integers.
		\item[(3)] $\mathbb{Q}=\{\frac{a}{b}:a,b\in \mathbb{Z}\,\,\text{and}\,\,b\neq 0\}$ is the set of all rational numbers.
		\item[(4)] $\mathbb{R} $ is the set of all real numbers.
		\item[(5)] $\emptyset \,\,\text{or}\,\,\{\}$ is the empty set. It has no elements.
	\end{enumerate}
\end{exa}

\section{New Sets from Old Sets}
When talking about a specific set, we need some way to explain to people what set we are talking
about. If it only has a few elements, we could simply list them all between curly brackets. So,
for example, the set of all natural numbers less than $10$ is $\{0, 1, 2, 3, 4, 5, 6, 7, 8, 9\}$. For sets with infinitely many (or even just very many) elements, this form of notation is no longer practical. 

If there is a well-established pattern then we can use dots to save space. 
\begin{exa}
	The set of all even natural numbers less than $99$ is $\{0, 2, 4, 6,\ldots, 96, 98\}$ and the set of all odd natural numbers is $\{1, 3, 5, 7, ...\}$.
\end{exa}
 Often the set we want to talk about is the set of all objects (elements) of a certain form that satisfy a certain condition (or that possess a certain property). Then we can use notation of the form $\{\text{thing} ~|~ \text{property}\}$ where the bar stands for the words "such that". 
 
 \begin{exa}
 	The set $\{n^2~ | ~n \in \mathbb{N}\}$ is the set of all numbers of the form $n^2$ such that $n$ is a natural number. So it is the set of all square natural numbers. 
 \end{exa}
 
 \begin{exa}
 	$\{a \in \mathbb{R} ~|~ a>0\}$ which is the set of all real numbers $a$ such that $a$ is bigger than zero. So it is the set of all positive real numbers.
 \end{exa}

Using this notation we can now describe some standard ways of defining new sets in terms
of sets that we already know about.

\textbf{Intersection:} Let $A$ and $B$ be sets. Then $A\cap B = \{a ~|~ a\in A\,\, \text{and}\,\, a \in B\}$ (which could
also be written as $\{a \in A~ |~ a \in B\}$ or as $\{a\in B~ |~ a \in A\}$).

\textbf{Union:} Let $A$ and $B$ be sets. Then $A\cup B = \{a ~| ~a \in A \,\,\text{or}\,\, a \in B\}$.

\textbf{Relative Complement:} Let $A$ and $B$ be sets. Then $A\setminus B = \{a~ | ~a \in A \,\,\text{and}\,\, a \notin B\}$ (which could also be written as $\{a \in A ~|~ a \notin B\}$)

\textit{(Recall how to prove De Morgan's laws theoretically.)}

\textbf{Cartesian Product:} Let $A$ and $B$ be sets. Then $A\times B = \{(a, b) ~| ~a\in A\,\, \text{and}\,\, b \in B\}$. Here $(a, b)$ is the ordered pair whose first coordinate is $a$ and whose second coordinate is $b$.
