\contentsline {chapter}{Notation}{i}{chapter*.1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Sets}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Sets}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}New Sets From Old Sets}{2}{section.2.2}
\contentsline {chapter}{\numberline {3}Relations}{4}{chapter.3}
\contentsline {section}{\numberline {3.1}Functions And Relations}{4}{section.3.1}
\contentsline {section}{\numberline {3.2}New Functions From Old Functions}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Equivalence Relations, Congruences And Partitions}{8}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\unskip \nobreakspace {}Equivalence Relation.\nobreakspace {}}{8}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Congruence Relation}{9}{section.3.4}
\contentsline {subsection}{\numberline {3.4.6}\unskip \nobreakspace {}Partitions.\nobreakspace {}}{10}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}Binary Operations.}{12}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}\unskip \nobreakspace {}Isomorphic Binary Structures.\nobreakspace {}}{12}{subsection.3.5.1}
\contentsline {chapter}{\numberline {4}The Second Squared Chapter}{13}{chapter.4}
\contentsline {chapter}{References}{15}{chapter*.4}
