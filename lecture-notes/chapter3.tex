\chapter{Relations}
\section{Functions and Relations}
\begin{defn}[Relation]
	For sets $A$ and $B$, a \textit{relation} between $A$ and $B$ is any subset of the of the Cartesian product $A\times B$. In the case where $A=B$, we speak of $R$ being a relation in $A$.
\end{defn}
\begin{rem}
	Since $A\times B$ and $B\times A$ are not generally the same sets ($i.e\,\, A\times B \neq B\times A$), a \textit{relation} between $A$ and $B$ maybe quite a different mathematical object from a \textit{relation} between $B$ and $A$.
\end{rem}

\begin{defn}[Domain and range]
	Let $R\subseteq A\times B$ be a relation. The \textit{domain} of $R$ (dom$R$) is defined by 
	$$\text{dom}R=\{a\in A|(a,b)\in R\,\,\text{for some }\,\, b\in B\}$$
	and the \textit{range} of $R$ (rng$R$) is defined by 
	$$\text{rng}R=\{b\in B|(a,b)\in R\,\,\text{for some}\,\, a\in A\}$$
\end{defn}

\begin{rem}
	\begin{enumerate}
		\item[(a)] dom$R\subseteq A$, rng$R\subseteq B$ and $R\subseteq \text{dom}R\times\text{rng}R$.
		\item[(b)] it may happen that dom$R=A$, then $R$ is called a \textit{relation} from $A$ into $B$.
	\end{enumerate}
\end{rem}
 
\begin{defn}[Function or Map]
	A function or mapping $f$ from a set $A$ into a set $B$ is a relation $f$ from $A$ into $B$  such that whenever, $(a,b_1)\in f$ and $(a,b_2)\in f$, then $b_1=b_2$. In other words no two distinct ordered pairs of $f$ have the same first component.
\end{defn}

(\textit{Recall also the Vertical Line Test (VLT) in first year}).
\begin{exa}
	The set of ordered pairs $R=\{(x,y)|x,y\in \mathbb{R}|x^2+y^2=25\}$ is a relation in $\mathbb{R}$, but not a function.
	
	\textit{\textbf{Why?}} Since $x^2+y^2=25$ is the equation of a circle with radius $5$ centred at the origin, by the \textit{Vertical Line Test}, the line must intersect the graph of $x^2+y^2=25$ in at most one point, which is not the case here.
	
\end{exa}
\begin{rem}
\begin{enumerate}
	\item[(i)] A function is a relation from $A$ into $B$ in which each $a\in A$ is related to exactly one $b\in B$.  
	\item[(ii)] $b$ is called the functional value or image of $a$ under $f$, denoted $f(a)$.
	\item[(iii)] $f(a)$ is the unique second component of $f$ having $a$ as first component.
	\item[(iv)] $f=\{(a,f(a))|a\in A \}$.
	\item[(v)] To convey the information that $f$ is a function from $A$ into $B$, we write $f:A\longrightarrow B$; in particular this signifies that dom$f=A.$
	\item[(vi)] For most functions $f:A\longrightarrow B$ it is not the case that rng$f=B$. And this will lead to the definition of Surjectivity (onto).
\end{enumerate}
\end{rem}
We shall be particularly interested in three properties that a function might have.

Let $f:A\longrightarrow B$ be a function, then;
\begin{defn}[Injective or One-to-one]
	A function $f$ is called \textit{one-to-one (injective)} if and only if for all $a_1,a_2\in A$ with $a_1\neq a_2$, implies that $f(a_1)\neq f(a_2)$, i.e. distinct elements in the domain have distinct functional values. Similarly, for all $a_1,a_2\in A$, if $f(a_1)= f(a_2)$, then $a_1=a_2$.
\end{defn}

\textit{\textbf{How do we check for one-to-oneness ?}}

\textbf{\underline{ALGEBRAICALLY}}
\begin{enumerate}
	\item[(1)] Suppose that $x_1,x_2\in\text{dom}f,\,\, x_1\neq x_2$, show that $f(x_1)\neq f(x_2)$ \textbf{OR}
	\item[(2)] suppose that $f(x_1)=f(x_2)$, show that $x_1=x_2$ for all $x_1,x_2\in\text{dom}f$.
	
	If $f$ is a continuous function, then $f$ is \textit{one-to-one} iff it is strictly increasing, which is equivalent to the derivative been always positive or always negative in the case where $f$ is differentiable.
\end{enumerate}
\textbf{\underline{GRAPHICALLY:}}

\textit{(Recall the Horizontal Line Test (HLT).)}

$f$ is \textit{one-to-one} iff every horizontal line intersects the graph of $f$ in at most one point.

\begin{defn}[Surjective or Onto]
	A function $f$ is said to be \textit{surjective (onto)} if the following condition is satisfied. For all $b\in B$ there exists at least one $a\in A$ such that $f(a)=b$.
\end{defn}

\begin{defn}[Bijection]
	We say that $f$ is a bijection ($f$ is bijective) if $f$ is both \textit{injective} and \textit{surjective}.
\end{defn}

\begin{enumerate}
	\item[(i)] Two functions $f$ and $g$ are identical iff they are composed of the same ordered pairs.
	\item[(ii)] Equivalently, $f=g$ iff dom$f=\text{dom}g$ and $f(a)=g(a)$ for each $a$ in their common domain.
\end{enumerate}
\section{New Functions from Old  Functions}
We shall also sometimes be concerned with composition of functions. 

Suppose that $f$ and $g$ are functions whose ranges are a subset of a system in which addition, multiplication and division  are permissible, then we can define functions $f+g$, $f\cdot g$ and $\frac{f}{g}$ as;
\begin{align*}
(f+g)(a)&=f(a)+g(a)\\
(f\cdot g)(a)&=f(a)\cdot g(a)\\
\bigg(\frac{f}{g}\bigg)(a)&=\frac{f(a)}{g(a)},\,\,\, \text{Where}, \,\,\,g(a)\neq 0 \,\,\, \text{and,}\,\,\, a\in\text{dom}f\cap\text{dom}g.
\end{align*}
$f+g$ is called the \textit{pointwise sum} and $f\cdot g$ is called the \textit{pointwise product} of $f$ and $g$.

\begin{defn}[Composition of Functions]
	Let $A,B$ and $C$ be sets. Let $g:A\longrightarrow B$ and $f:B\longrightarrow C$. Then $f\circ g: A\longrightarrow C$ is called a composite function defined as follows. For all $a\in A,\,\,(f\circ g)(a)=f(g(a))$.
\end{defn}
In other words, the \textit{composition} of two functions $f$ and $g$ denoted $f\circ g$ is defined by $$f\circ g=\{(a,b)|\text{for some}\,\, c,\,(a,c)\in g\,\,\, \text{and}\,\,\,(c,b)\in f\}$$
That is; $(f\circ g)(a)=f(g(a))$ where, $a\in$ dom$g$ and $g(x)\in$ dom$f$. 
\begin{note}
	that $\text{dom}(f\circ g)\subseteq \text{dom}(g)$ and rng$(f\circ g)\subseteq \text{rng}(f)$.
\end{note}

\begin{enumerate}
	\item[(1)] Suppose that $f=\{(x,\sqrt{4-x^2}),\,\,-2\leq x \leq 2\}$ and $g=\{(x,\frac{2}{x}) |x\in\mathbb{R}\backslash \{0\}\}$, $i.e$. $f(x)=\sqrt{4-x^2}$ for $-2\leq x \leq 2$ and $g(x)=\frac{2}{x}$ for $x\in\mathbb{R}\backslash \{0\}$. If $x\in\text{dom}f\cap\text{dom}g=[-2,0)\cup (0,2]$. 
	
	Then, 
	\begin{align*}
	(f+g)(x)&=f(x)+g(x)=\sqrt{4-x^2}+\frac{2}{x}\\
	(f\cdot g)(x)&=f(x)\cdot g(x)=(\sqrt{4-x^2})\bigg(\frac{2}{x}\bigg)=\frac{2\sqrt{4-x^2}}{x}
	\end{align*}
	
	\textit{\underline{Homework; (Hint: Use definition of composite functions)}}
	\item[(2)] Suppose that $f=\{(x,\sqrt{x}) |x\in\mathbb{R},x\geq 0\}$ and $g=\{(x,2x+3)|x\in\mathbb{R}\}$ that is $f(x)=\sqrt{x}$, for all $x\geq 0$ and $g(x)=2x+3$ for all $x\in\mathbb{R}$.
	
	Then find;
	\begin{itemize}
		\item[(i)] $(f\circ g)(x)$  where, dom$(f\circ g)=\{x\in\text{dom}g|g(x)\in\text{dom}f\}$
		\item[(ii)] $(g\circ f)(x)$  where, dom$(g\circ f)=\{x\in\text{dom}f|f(x)\in\text{dom}g\}$
	\end{itemize}
\end{enumerate}
\begin{defn}[Inverse of a Function]
	The inverse of a one-to-one function $f$, written $f^{-1}$, is the set of ordered pairs
	$$f^{-1}=\{(b,a)|(a,b)\in f\}$$
\end{defn}

\begin{enumerate}
	\item[(i)] dom$f^{-1}=\text{rng}f$ and rng$f^{-1}=\text{dom}f$
	\item[(ii)] Cancellation laws;
	\begin{align*}
	(f^{-1}\circ f)(x)&=x,\,\,\,\text{for}\,\,\,x\in\text{dom}f\\
	(f\circ f^{-1})(y)&=y,\,\,\,y\in\text{dom}f^{-1}=\text{rng}f
	\end{align*}
\end{enumerate}
\begin{pro}
	Let $A, B$ and $C$ be sets. Let $g : A \longrightarrow B$ and $f : B \longrightarrow C$
	\begin{enumerate}
		\item[(i)] If $f$ and $g$ are injective then $f\circ g$ is injective.
		\item[(ii)] If $f$ and $g$ are surjective then $f\circ g$ is surjective.
		\item[(iii)] If $f$ and $g$ are bijective then $f\circ g$ is bijective.
		\item[(iv)] If $g$ is a bijection then there is exactly one bijection $h:B\longrightarrow A$ such that, for all $a\in A$ and $b\in B$, $(h\circ g)(a) = a$ and $(g\circ h)(b) = b$.
	\end{enumerate}
\begin{proof}
	We shall discuss the proof during the lecture time.
\end{proof}
\end{pro}

The unique bijection $h$ described in item $(iv)$ of Proposition $3.2.2$ is called the inverse of $g$ and is denoted $g^{-1}$.

\begin{defn}[Identity Function]
	Given a nonempty set $X$, The function $i_x:X\longrightarrow X$ defined by $i_x(x)=x$ for each $x\in X$ is called the identity function on $X$.
\end{defn}
\begin{rem}
	\begin{enumerate}
		\item[(i)] $f^{-1}\circ f=i_x$
		\item[(ii)] $f\circ f^{-1}=i_y$.
	\end{enumerate}
\end{rem}

\begin{exa}
	The function $f=\{(x,3x-2)|x\in\mathbb{R}\}$ is \textit{one-to-one} (Show). Thus, the inverse $f^{-1}$ exists and is the set $f^{-1}=\{(3x-2,x)|x\in\mathbb{R}\}$. However, it is preferable to write $f^{-1}$ in terms of its domain and the value at each point of the domain.
	
	Thus, 
	\begin{align*}
	f^{-1}&=\{(x,\frac{1}{3}(x+2))|x\in\mathbb{R}\}\,\,\,\text{\textbf{OR}}\\
	f^{-1}(x)&=\frac{1}{3}(x+2)\,\,\,\text{for each}\,\,\,x\in\mathbb{R}
	\end{align*}
\end{exa}

\begin{defn}[Direct Image and Inverse Image]
	Consider a function $f:X\longrightarrow Y$. If $A\subseteq X$, then the \textit{direct image} of $A$ under $f$, $f(A)$ is the subset of $Y$ defined by $$f(A)=\{f(x)|x\in A\}$$
	If $B\subseteq Y$, then the \textit{inverse image} of $B$ under $f$ denoted by $f^{-1}(B)$ is the subset of $X$ defined by $$f^{-1}(B)=\{x|f(x)\in B\}.$$
\end{defn}
\begin{rem}
	\begin{enumerate}
		\item[(i)] The inverse image of  a subset of $Y$ is \textbf{NOT} the same as the inverse function defined earlier.
		
		\textit{for example;} Let $f:\mathbb{R}\longrightarrow \mathbb{R}$ be defined by $f(x)=x^2$. Then, $f^{-1}(\{4\})=\{-2,2\}$.
		
		\textbf{note;} $f$ doesn't have an inverse function. (\textit{\textbf{Why?} demostrate graphically using HLT})
		\item[(ii)] The inverse image is a set, whereas if $f$ had an inverse function $f^{-1}$, then $f^{-1}(a)=b$ where $b\in\text{dom}f$ such that $f(b)=a.$
	\end{enumerate}
\end{rem}



\textbf{\underline{Some Useful Results}}
\begin{thm}
	If $f:X\longrightarrow Y$, then for each subset $B\subseteq Y\,\,\,f(f^{-1}(B))\subseteq B$.
	\begin{proof}
		Suppose that $f:X\longrightarrow Y$ is a function and $B\subseteq Y$. We prove that $f(f^{-1}(B))\subseteq B.$ Let $c\in f(f^{-1}(B))\Rightarrow c=f(b)$ for some $f(b)\in B$. And so, $c\in B.$ Therefore, $f(f^{-1}(B))\subseteq B.$
	\end{proof}
\end{thm}

\begin{thm}\label{1}
	If $f:X\longrightarrow Y$ then for each subset $A\subseteq X$, $A\subseteq f^{-1}(f(A))$.
	\begin{proof}
		Suppose that $f:X\longrightarrow Y$ is a function and $A\subseteq X$, we prove that $A\subseteq f^{-1}(f(A)).$ 
		
		Let, $a\in A$, then since
		\begin{align*}
		f^{-1}(f(A))&=\{x|f(x)\in f(A)\}\\
		&=\{x|f(x)\in\{f(a)|a\in A\}\}
		\end{align*}
		but $a\in A$, so $f(a)\in f(A)$ thus, $a\in f^{-1}(f(A))$. Hence, $A\subseteq f^{-1}(f(A)).$
	\end{proof}
\end{thm}
\begin{cor}\label{2}
	If  in addition, $f$ is a \textit{one-to-one} function, then $A=f^{-1}(f(A))$.
	\begin{proof}
		We already have that $A\subseteq f^{-1}(f(A)).$ Required to prove (RTP) that $ f^{-1}(f(A))\subseteq A.$ Let $x\in f^{-1}(f(A))$ then $f(x)\in f(A)\Rightarrow f(x)=f(a)$ for some $a\in A$, but $f$ is \textit{one-to-one} function, so $x=a\in A$. Thus, $f^{-1}(f(A))\subseteq A.$ Therefore, by Theorem \ref{1} and the result in Corollary \ref{2}, we conclude that $A=f^{-1}(f(A))$.
	\end{proof}
\end{cor}

\section{Equivalence Relations, Congruences and Partitions}
\subsection{Equivalence Relation}
\begin{defn}[Equivalence Relation]
	A relation $R$ in a set $X$ is an \textit{equivalence relation} in $X$ if it satisfies the following properties:
	\begin{enumerate}
		\item[(i)] \textit{Reflexive property;} $(a,a)\in R$ for all $a\in X$.
		\item[(ii)] \textit{Symmetric property;} if $(a,b)\in R,$ then $(b,a)\in R$,  for all $a,b\in X$
		\item[(iii)] \textit{Transitive property;} if $(a,b)\in R$ and $(b,c)\in R$, then $(a,c)\in R$, for all $a,b,c\in X$.
	\end{enumerate}
\end{defn}

\begin{rem}
	\begin{enumerate}
		\item[(i)] Equivalence relations are denoted by $\sim$.
		\item[(ii)] $(a,b)\in \sim$ denoted $a\sim b$.
	\end{enumerate}
\end{rem}

\begin{exa}[Discuss solutions during the lecture]
	\begin{enumerate}
		\item[(1)] Let $L$ be the set of all lines in a plane, \textit{"is parallel to"} defines an equivalence relation in $L$.
		
		\textit{(We agree here that any line is parallel to itself)}
		\item[(2)] Let $\mathcal{F}$ be an arbitrary family of functions from $X$ into $Y$, then interpreting $a\sim b$ to mean $f(a)=f(b)$ for every $f\in\mathcal{F}$ gives an equivalence relation in $X$. \textit{(Check !!!)}
	\end{enumerate}
\end{exa}

Any equivalence relation $\sim$ determines a separation of the set $X$ into a collection of subsets of a particular kind:

\begin{defn}[Equivalence Class Determined by $a$]
	For each $a\in X$, the set $[a]=\{b\in X|b\sim a\}$ is referred to as the \textit{equivalence class} determined by $a.$
\end{defn}
Let $X/\sim=\{[a]|a\in X\},\,\,i.e.\,\,X/\sim$ is the set of \textit{equivalence classes} of the relation $\sim.$ Then we have :

\begin{thm}[Properties of Equivalence Classes]
	\begin{enumerate}
		\item[(i)] For each $a\in X,\,\, [a]\neq \emptyset$.
		\item[(ii)] If $b\in [a],$ then $[a]=[b].$
		\item[(iii)] For all $a,b\in X,\,\,[a]=[b]$ iff $a\sim b.$
		\item[(iv)] For all $a,b\in X$ either $[a]\cap [b]=\emptyset$ or $[a]=[b]$.
		\item[(v)] $\bigcup X/\sim=X.$
	\end{enumerate}
\begin{proof}
	\begin{enumerate}
		\item[(i)] By Reflexivity, for all $a\in X$, we have $a\sim a$ thus, $a\in [a]$. And so $[a]\neq \emptyset$.
		\item[(ii)] Suppose that $b\in[a],$ then $b\sim a$,
		
		\quad \quad Required To Prove (RTP): $[a]=[b]\,\,\,i.e.\,\,[a]\subseteq [b]$ and $[b]\subseteq [a].$
		
		Showing that $[a]\subseteq [b]$. Let $c\in [a]$, then $c\sim a$ by Definition of equivalence class. But $b\sim a=a\sim b$ \textit{(by symmetric property)} and $c\sim a$ and $a\sim b\Rightarrow c\sim b\Rightarrow c\in[b].$ Hence, $[a]\subseteq [b]$ as required.
		
		\textit{Conversely:}(Try using the same argument)
		
		\textit{Hint!} Show that $[b]\subseteq [a]$, then conclude that $[a]=[b].$
		\item[(iii)] Suppose $a,b\in X$ and $[a]=[b]$,
		
		\quad \quad RTP: $a\sim b$. Let $a\in[a]=[b]$, so $a\in [b]\Rightarrow a\sim b.$ 
		
		\textit{Conversely}
		
		Suppose $a\sim b$ for some $a,b\in X$.
		
		\quad \quad RTP: $[a]=[b]\Rightarrow a\in[b] \Rightarrow [a]=[b]$ \textit{(by $(ii)$)}
		\item[(iv)] Let $a,b\in X$ and suppose $[a]\cap [b]=\emptyset$,
		
		\quad \quad RTP: $[a]=[b]$.
		
		There exists $c\in X$ such that $c\in [a]$ and $c\in [b]\Rightarrow c\sim a$ and $c\sim b.$ By symmetry $a\sim c$ and $c\sim b$, so by transitivity, $a\sim b$. Thus, by $(ii)\,\,\, [a]=[b]$ as required.
		\item[(v)] Let $a\in[a]$ for all $a\in X$, then $X=\bigcup_{a\in X}[a]=\bigcup X/\sim$.
		
	\end{enumerate}
\end{proof}
\end{thm}
\begin{exa}
	Shows that any mapping determines an equivalence relation in its domain.
\end{exa}

The following corollary indicates that every equivalence relation arises in this manner, $i.e.$ each equivalence relation is the associated equivalence relation of some function.

\begin{cor}
	Let $\sim$ be an equivalence relation in the set $X$. Then there exists a set $Y$ and a mapping $g:X\longrightarrow Y$ such that $a\sim b$ iff $g(a)=g(b)$.
	\begin{proof}
		Take $Y=X/\sim$ and $g:X\longrightarrow Y$ to be defined by $g(a)=[a]\,\,i.e.\,\,\,g$ sends every element to the unique equivalence class to which it belongs. By the previous Theorem 4.1.2 $a\sim b$ iff $[a]=[b]$ or equivalently, $g(a)=g(b)$.
	\end{proof}
\end{cor}
\section{Congruence Relation}
The notion of congruence modulo $m$ was invented by \textit{Karl Friedrich Gauss}, and does much to simplify arguments about divisibility.

\begin{defn}
	Let $a,b,m\in\mathbb{Z}$, with $m>0$. We say that $a$ is \textit{congruent} to $b$ \text{modulo} $m$, written $$a\equiv b\,\,(\text{mod}\,\,m),$$ if $m|(a-b)$. We call $m$ a \text{modulus} in this situation. If $m\nmid (a-b)$, we say that $a$ \text{is incongruent to} $b$ \text{modulo} $m$, written $$a\not\equiv b\,\,(\text{mod}\,\,m).$$
\end{defn}
\begin{exa}
	\begin{enumerate}
		\item[(1)] If $m=11$. We have $-1\equiv 10\,\,(\text{mod}\,\,11)$, since $11|(-1-10)=-11$. We have $108\not\equiv 7\,\,(\text{mod}\,\,11)$, since $11\nmid (108-7)=101$.
		\item[(2)] If $m=2$. When do we have $a\equiv b\,\,(\text{mod}\,\,2)$? We must have $2|(a-b)$. In other words, $a-b$ must be even. This is true iff $a$ and $b$ has the same \textit{parity}: $i.e.$ iff both are even or both are odd.
		\item[(3)] $m=1$, shows that for any $a$ and $b$ we have $a\equiv b\,\,(\text{mod}\,\,1)$. Since $1|(a-b)$.
		\item[(4)] When do we have $a\equiv 0\,\,(\text{mod}\,\,m)$? This is only true iff $m|(a-0)$. Thus the connection with divisibility: $m|a$ iff $a\equiv0\,\,(\text{mod}\,\,m)$.
	\end{enumerate}
\end{exa}

Congruence is meant to simplify discussions of divisibility, and yet in our examples we had to use divisibility to prove congruences. The following theorem corrects this.

\begin{thm}
	Let $a,b,m\in\mathbb{Z}$ with $m>0$. Then $a\equiv b\,\,(\text{mod}\,\,m)$ if and only if (iff) there is a $k\in \mathbb{Z}$ such that $b=a+km$.
	\begin{proof}
		We have $a\equiv b\,\,(\text{mod}\,\,m)$ iff $m|(a-b)$. By definition this is true iff  there is a $k\in\mathbb{Z}$ such that $a-b=km$, which is true iff $a=b+km$ for some $k\in\mathbb{Z}$.
	\end{proof}
\end{thm}
Given a positive integer $a$ and a modulus $m$, to list all integers congruent to $a$ modulo $m$. We simply take the set $\{a + km : k \in \mathbb{Z}\}$.

\begin{exa}
	Take $m=3.$ Then
	\begin{enumerate}
		\item[(1)] The set of all integers congruent to $0$ modulo $3$ is $\{0+k3:k\in\mathbb{Z}\}=\{...,-6,-3,0,3,6,9,...\}$.
		\item[(2)] The set of all integers congruent to $1$ modulo $3$ is $\{1+k3:k\in\mathbb{Z}\}=\{...,-5,-2,1,4,7,10,...\}$.
		\item[(3)] The set of all integers congruent to $2$ modulo $3$ is $\{2+k3:k\in\mathbb{Z}\}=\{...,-4,-1,2,5,7,12,...\}$.
	\end{enumerate}
\end{exa}

Congruence modulo $m$ defines a binary relation on $\mathbb{Z}$. One property that makes this such a useful relation is that it is an equivalence relation!

\begin{thm}
	Let $m\in\mathbb{Z}^+$ and consider the relation $R$ defined by $a,R,b,\,\,\text{if and only if}\,\,\, a\equiv b\,\,(\text{mod}\,\,m).$ Then $R$ is an equivalence relation.
	\begin{proof}
		We show that $R$ is \textit{Reflexive, Symmetric} and \textit{Transitive.} That is;
		\begin{enumerate}
			\item[(i)] $R$ \textit{is reflexive:} for all $a\in \mathbb{Z}$ we have $a\equiv a\,\,(\text{mod}\,\,m).$ 
			
			\textit{proof.}
			
			Since $m|(a-a)=0$, we have $a\equiv a\,\,(\text{mod}\,\,m)$. Therefore, \textit{Reflexive property} holds.
			
			\item[(ii)] $R$ \textit{is symmetric:} if $a\equiv b\,\,(\text{mod}\,\,m)$, then $b\equiv a\,\,(\text{mod}\,\,m).$
			
			\textit{proof.}
			
			If $m|(a-b)$, then $m|(-1)(a-b)=m|(b-a)$. Thus, $a\equiv b\,\,(\text{mod}\,\,m)$ implies $b\equiv a\,\,(\text{mod}\,\,m)$.
			
			\item[(iii)] $R$ \textit{is transitive:} if $a\equiv b\,\,(\text{mod}\,\,m)$ and $b\equiv c\,\,(\text{mod}\,\,m)$, then $a\equiv c\,\,(\text{mod}\,\,m)$.
			
			\textit{proof.}
			
			Suppose that $a\equiv b\,\,(\text{mod}\,\,m)$ and $b\equiv c\,\,(\text{mod}\,\,m)$. Then by the previous theorem, we can write $b=a+km$ for some $k\in\mathbb{Z}$ and $c=b+k'm$ for some $k'\in\mathbb{Z}$. 
			
			But then $c=b+k'm=a+km+k'm=a+(k+k')m=a+qm$, where $q=k+k'\in\mathbb{Z}$.
			
			Thus, $a\equiv c\,\,(\text{mod}\,\,m)$. By \textit{(i),(ii)} and \textit{(iii)} we conclude that $R$ is an equivalence relation.
		\end{enumerate}
	\end{proof}
\end{thm}

\subsection{Partitions}
\begin{defn}[Partition of a Set]
	By a \textit{partition} of a set $X$ is meant a family $\mathcal{F}$ of subsets of $X$ with that properties:
	\begin{enumerate}
		\item[(i)] $\emptyset\notin \mathcal{F}$.
		\item[(ii)] for any $A,B\in\mathcal{F}$, either $A=B$ or $A\cap B=\emptyset$ \textit{(pointwise disjoint).}
		\item[(iii)] $\bigcup \mathcal{F}=X$
	\end{enumerate}
\end{defn}

\begin{rem}
	\begin{enumerate}
		\item[(i)] A partition of $X$ is a collection $\mathcal{F}$ of non-empty subsets of $X$ such that every element of $X$ belongs to one and only one member of $\mathcal{F}$. 
		\item[(ii)] Theorem 4.1.2, asserts that each equivalence relation $\sim$ on a set $X$ yields a partition of $X$, namely the partition $X/\sim$ into the equivalence classes of $\sim$.
	\end{enumerate}
\end{rem}
\begin{exa}
	\begin{enumerate}
		\item[(1)] $\mathbb{Z}$ can be partitioned into subsets of even and odd integers. 
		\item[(2)] $\mathbb{Z}$ can be partitioned into positive integers, negative integers and $\{0\}$.
		\item[(3)] Consider the relation on $\mathbb{Z}:\,\,\, a\sim b$ iff $a=b$ \textit{(Check that $"\sim"$ is an equivalence relation !!!)}. What are the equivalence classes? 
		
		\textit{(Discuss solution during the lecture).}
	\end{enumerate}
\end{exa}

We can show that a given partition of $X$ induces an equivalence relation in $X$. To do this, we need the following lemma:

\begin{lem}
	Two equivalence relations $\sim$ and $\sim'$ in the set $X$ are the same iff $X/\sim$ and $X/\sim'$ are the same.
	\begin{proof}
		Suppose that $\sim$ and $\sim'$ are the same, then surely  $X/\sim$ and $X/\sim'$ are the same.
		
		Now suppose there exists elements $a,b\in X$ such that $a\sim b$ but $a\sim'b$ does not hold. Then by Theorem 4.1.2, there is an equivalence class in $X/\sim$ containing $a$ and $b$ and no such class in $X/\sim'$. Thus, $X/\sim$ and $X/\sim'$  differ.
	\end{proof}
\end{lem}

\begin{thm}
	If $\mathcal{F}$ is a partition of the set $X$, then there is a unique equivalence relation in $X$ whose equivalencIn general, given $n$ a positive integer then applying the congruence $a\sim b$ iff $a\equiv b(\text{mod}\,\,n)$, we have the following equivalence classes: $$[0],[1],[2],...,[n-1]$$
	Take $\mathbb{Z}_n$ to be thee classes are precisely the members of $\mathcal{F}.$
	\begin{proof}
		Since $\mathcal{F}$ is a partition, each element of $X$ lies in precisely one member of $\mathcal{F}$.  Let $a,b\in X$ and define $a\sim b$ iff $a$ and $b$ belong to the same subset of $\mathcal{F}$. Now we prove that $X/\sim=\mathcal{F}$. Suppose $P\in \mathcal{F}$, then $a\in P$ for some $a\in X$. Now $b\in P$ iff $b\sim a$ iff $b\in [a]$. Thus, $P=[a]\in X/\sim$, so $\mathcal{F}\subseteq X/\sim$.
		
		Let $[a]$ be an arbitrary equivalence class and $P$ be the partition set in $\mathcal{F}$ to which $a$ belongs. By a similar reasoning $[a]=P$. Hence, $X/\sim\subseteq\mathcal{F}$. Thus, the set of equivalence classes for equivalence classes for $\sim$ concide with the partition $\mathcal{F}$. The uniqueness follows from the \textit{lemma 4.2.2} above.
	\end{proof}
\end{thm}

\begin{exa}
	Let $X=\mathbb{R}\times\mathbb{R}$, define $\sim$ in $X$  by $(a,b)\sim (c,d)$ iff $a-c=b-d$ and if $(a,b)\in X$,
	
	then $$[(a,b)]=\{(x,y)|x-a=y-b\}$$
	This set maybe represented geometrically as a straight line with slope $1$ and passing through $(a,b)$. Thus, $\sim$ partitions $X$ into a family of parallel lines.
\end{exa}
\begin{exa}
	Find all equivalence relations on a three element set. 
	
	\textit{Solution.}
	
	Let $X=\{a,b,c\}$, then the possible partitions  and equivalence relations respectively are;
	\begin{align*}
	\big\{\{a,b,c\}\big\}&\Longrightarrow \big\{(a,a),(b,b),(c,c),(a,c),(c,a),(a,b),(b,a),(b,c),(c,b)\big\}\\
	\big\{\{a\},\{b\},\{c\}\big\}&\Longrightarrow \big\{(a,a),(b,b),(c,c)\big\}\\
	\big\{\{a,b\},\{c\}\big\}&\Longrightarrow \big\{(a,a),(b,b),(c,c),(a,b),(b,a)\big\}\\
	\big\{\{a,c\},\{b\big\}\}&\Longrightarrow \big\{(a,a),(b,b),(c,c),(a,c),(c,a)\big\}\\
	\big\{\{b,c\},\{a\big\}\}&\Longrightarrow \{(a,a),(b,b),(c,c),(b,c),(c,b)\}
	\end{align*}
	$a\sim b$ if and only if (iff) $a,b$ belong to the same element of $\mathcal{F}$ the partion.
	
	Here $\mathcal{F}=\big\{\{a,b,c\},\{a\},\{b\},\{c\},\{a,b\},\{c\},\{a,c\},\{b\big\},\{b,c\},\{a\big\} \big\}$
\end{exa}
\begin{exa}[very important example !!!]
	For integers $a$ and $b$ and $n$ a positive integer, define $a\sim b$ iff $a\equiv b(\text{mod}n)\,\,\,i.e.$ ($a$ is congruent to $b\,\,\text{modulo}\,\,n)\,\,\,i.e.$  iff $n|a-b$ iff $a=b+nk$ for some $k\in \mathbb{Z}$.
	
	Let $n=5$. Then for $a,b\in \mathbb{Z}, \,\,\,\,a\sim b$ iff $a\equiv b(\text{mod}\,\,5)$, Thus, we have the following equivalence classes:
	\begin{align*}
	[0]&=\{...,-10,-5,0,5,10,15,20...\}\\
	[1]&=\{...,-9,-4,1,6,11,16,21,...\}\\
	[2]&=\{...,-8,-3,2,7,12,17,...\}\\
	[3]&=\{...,-7,-2,3,8,13,18,...\}\\
	[4]&=\{...,-6,-1,4,9,14,19,...\}
	\end{align*}
\end{exa}

In general, given $n$ a positive integer then applying the congruence $a\sim b$ iff $a\equiv b(\text{mod}\,\,n)$, we have the following equivalence classes: $$[0],[1],[2],...,[n-1]$$
Take $\mathbb{Z}_n$ to be the set of these equivalence  classes as elements, $i.e.$
\begin{align}
\mathbb{Z}_n=\{0,1,2,...,n-1\}
\end{align}
This set will be very important in subsequent sections. 

\chapter{Binary Operations.}
\section{Binary Operation}
\begin{defn}[Binary Operation]
	Let $A$ be a non-empty set. A \textit{binary operation} $\star$ on a set $A$ is a function $\star:A\times A\longrightarrow A,~(a,b)\mapsto a\star b~\forall a,b\in A$. That is for each $(a,b)\in A\times A$, we will denote $\star ((a,b))\in A$ by $a\star b$.
\end{defn}
\begin{defn}[Closed Binary Operation]
	Let $\star$ be a binary operation on $A$ and $H\subseteq A$. Then $H$ is said to be \textit{closed} under $\star$ if for all $a,b\in H,~a\star b\in H$.
	
	The binary operation on $H$ given by restriction $\star$ to $H$ is called the \textit{induced operation } of $\star$ on $H$.
\end{defn}
\begin{exa}
	The operation $+$ on $\mathbb{R}$ does not induce a binary operation of $\star$ on $\mathbb{R}^*$, where $\mathbb{R}^*=\mathbb{R}\backslash \{0\}.$
\end{exa}
\begin{exa}
	Consider the set $\mathbb{Z}$ with its usual addition $(+)$ and multiplication $(\cdot)$. Define \[H:=\{n^2|n\in \mathbb{Z}\}.\] Is $H$ closed under $+$ and $\cdot$ ?
\end{exa}
\begin{defn}[Commutative Binary Operation]
	A binary operation $\star$ on $A$ is \textit{commutative} if and only if $a\star b=b\star a~\forall a,b \in A$.
\end{defn}
\begin{defn}[Associative Binary Operation]
	A binary operation $\star$ on a set $A$ is \textit{associative} if $(a\star b)\star c=a\star(b\star c)~\forall a,b,c\in A$.
\end{defn}
\section{Tables}
Binary operations can be represented in a very useful way by means of tables
\begin{exa}
	Let $A=\{a,b,c\}$ and let $\star$ be a binary operation defined by 
	\begin{align*}
		\begin{tabular}{|c|c|c|c|}
		\hline 
		$\star$ & $a$ & $b$ & $c$ \\ 
		\hline 
		$a$ & $b$ &$c$  & $b$  \\
		\hline
		$b$ & $a$ & $c$ & $b$\\
		\hline
		$c$ & $c$ & $b$ & $a$\\ 
		\hline 
		\end{tabular} 
	\end{align*} 
	Is $\star$ commutative ?
\end{exa}

\begin{exa}
	Let $S=\{a,b,c,d\}$ and let $\star$ be a binary operation defined by 
	\begin{align*}
	\begin{tabular}{|c|c|c|c|c|}
	\hline 
	$\star$ & $a$ & $b$ & $c$& $d$\\ 
	\hline 
	$a$ & $b$ &  & &  \\
	\hline
	$b$ & $d$ & $a$ &  &\\
	\hline
	$c$ & $a$ & $c$ & $d$ &\\
	\hline
	$d$ & $a$ & $b$ & $b$ & $c$ \\
	\hline 
	\end{tabular} 
	\end{align*} 
	Complete the table so that $\star$ is commutative. Notice symmetry along the diagonal.
\end{exa}

\section{Isomorphic Binary Structures}
\begin{defn}[Homomorphism]
		Let $\big<A,\star\big>$ and $\big<A',\star '\big>$ be binary  algebraic structures (\textit{a set together with a binary operation}). A map $\phi: A\longrightarrow A'$ is said to be a \textit{homomorphism} from
		$A$ to $A'$ if it satisfies \[\phi(x\star y)=\phi(x)\star'\phi(y)~\forall x,y\in A.\]
\end{defn}
\begin{defn}[Isomorphism]
	Let $\big<A,\star\big>,~\big<A',\star '\big>$ be binary algebraic structures. An \textit{isomorphism} of $A$ with $A'$ is a \textit{one-to-one} function $\phi$ mapping $A$ \textit{onto} $A'$ denoted $\phi:A\longrightarrow A'$ such that \[\phi(x\star y)=\phi(x)\star'\phi(y)~\forall x,y\in A.\]
	We write $A\cong A'$ to mean that $A$ is isomorphic to $A'$.
\end{defn}
\begin{exa}\label{bo}
	Let $A=\{a,b,c\}$ and $A'=\{\#,\$,\&\}$ defined by
	\begin{align*}
			\begin{tabular}{|c|c|c|c|}
		\hline 
		$\star$ & $a$ & $b$ & $c$ \\ 
		\hline 
		$a$ & $c$ &$a$  & $b$  \\
		\hline
		$b$ & $a$ & $b$ & $c$\\
		\hline
		$c$ & $b$ & $c$ & $a$\\ 
		\hline 
		\end{tabular} 
		\quad~~~~~~\text{and}~~~~~~~~~
			\begin{tabular}{|c|c|c|c|}
		\hline 
		$\star'$ & $\#$ & $\$$ & $\&$ \\ 
		\hline 
		$\#$ & $\&$ &$\#$  & $\$$  \\
		\hline
		$\$$ & $\#$ & $\$$ & $\&$\\
		\hline
		$\&$ & $\$$ & $\&$ & $\#$\\ 
		\hline 
		\end{tabular} 
	\end{align*}
	Define $\phi: A\longrightarrow A'$ by $\phi(a)=\#,~\phi(b)=\$,~\phi(c)=\&$. Check if $\phi(a\star b)=\phi(a)\star'\phi(b)~\forall a,b\in A$.
\end{exa}

\textbf{How to Show that Binary Structures are Isomorphic}

Given two binary algebraic structures $\big<A\star\big>$ and $\big<A',\star '\big>$, then 
\begin{enumerate}
	\item[(i)] Define the function $\phi$ that gives the isomorphism of $A$ with $A'$. This function must be well defined, if $x=y~\forall x,y\in A\Longrightarrow\phi(x)=\phi(y)$.
	\item[(ii)] Show that $\phi$ is one-to-one, i.e. Suppose $\phi(x)=\phi(y)$ for all $x,y\in A$, then show that $x=y$. Or Suppose $x\neq y$ for some $x,y\in A$, then show that $\phi(x)\neq\phi(y)$.
	\item[(iii)] Show that $\phi$ is onto, i.e. Let $y\in A'$, then show that there exists $x\in A$ such that $\phi(x)=y$.
	\item[(iv)] Show that $\phi$ is homomorphism, i.e. $\phi(x\star y)=\phi(x)\star'\phi(y)~\forall x,y\in A$.
\end{enumerate}
\begin{exa}
	Show that $\big<\mathbb{R},+\big>\cong\big<\mathbb{R}^+,\cdot\big>$.
	\begin{enumerate}
		\item[(i)] Step 1; Define $\phi:\mathbb{R}\longrightarrow \mathbb{R}^+$ by $\phi(x)=e^x,~\forall x\in \mathbb{R}$. Since $e^x>0,~\forall x\in \mathbb{R}$, then $\phi$ maps $\mathbb{R}$ into $\mathbb{R}^+$.
		\item[(ii)] Step 2; Let $x_1,x_2\in\mathbb{R}$ and suppose $\phi(x_1)=\phi(x_2)$, then we are required to prove (RTP) that $x_1=x_2$. Thus, \begin{align*}
			e^{x_1}&=e^{x_2}\\
			In(e^{x_1})&=In(e^{x_2})\\
			\Rightarrow x_1&=x_2.
		\end{align*}
		\item[(iii)] Step 3; Let $y\in\mathbb{R}^+$, then we show that there exists $x\in\mathbb{R}$ such that $\phi(x)=y$. Consider $x=In(y)$, then $\phi(x)=e^x=e^{In(y)}=y$ as required.
		\item[(iv)] Step 4; Let $x,y\in \mathbb{R}$, then we show that $$\phi(x+ y)=\phi(x)\phi(y)~\forall x,y\in \mathbb{R}.$$
		Now \begin{align*}
			\phi(x+y)=e^{x+y}=e^x\cdot e^y=\phi(x)\phi(y).
		\end{align*} As required. Since $\phi$ satisfies conditions (i) to (iv). Therefore, $\big<\mathbb{R},+\big>\cong\big<\mathbb{R}^+,\cdot\big>$. 
	\end{enumerate}
	
\end{exa}
\begin{exe}
	Let $2\mathbb{Z}=\{2n|n\in \mathbb{Z}\}$, i.e. be the set of all even integers. Show that $\big<\mathbb{Z},+\big>\cong\big<2\mathbb{Z},+\big>$.
\end{exe}

\textbf{How to Show that Binary Structures are NOT Isomorphic.}
\begin{defn}[Structural Property]
	A \textit{structural property} of a binary structure is one that must be shared by any isomorphic structure.
\end{defn}
\begin{note}
	\begin{enumerate}
		\item[(i)]In the event that there are one-to-one mappings of $A$ onto $A'$, we usually show that $\big<A,\star\big>$ is not isomorphic to $\big<A',\star'\big>$ (if this is the case) by showing that one has some structural property that the other does not posses.
		\item[(ii)] If two structures $\big<A,\star\big>$ and $\big<A',\star'\big>$ have the same cardinality, then $\big<A,\star\big>\cong\big<A',\star'\big>$. Otherwise, $\big<A,\star\big>\ncong\big<A',\star'\big>$.
		\item[(iii)] Same cardinality sometimes does not necessarily imply isomorphic.  The following is a good counter example; 
	\end{enumerate}
	\begin{exa}
		The sets $\mathbb{Z}$ and $\mathbb{Z}+$ have the same cardinality $\mu$ and there are many one-to-one functions mapping $\mathbb{Z}$ into $\mathbb{Z}+$. However, $\big<\mathbb{Z},\cdot\big>$ and $\big<\mathbb{Z}^+,\cdot\big>$ are not isomorphic. Take $x\cdot x=x$, in $\mathbb{Z}$ has two solutions whilst in $\mathbb{Z}^+$ has only one solution.
	\end{exa}
\begin{exa}
	$\big<\mathbb{Q},+\big>$ and $\big<\mathbb{Z},+\big>$ are not isomorphic. [Both have cardinality $\mu$, so there are many one-to-one functions mapping $\mathbb{Q}$ onto $\mathbb{Z}$]. The equation $x+x=\alpha$ has a solution $x,~\forall\alpha\in \mathbb{Q}$, but in $\mathbb{Z}$ this is not the case; $x+x=3$ has no solution in $\mathbb{Z}$.
\end{exa}
\begin{exa}
	$\big<\mathbb{C},+\big>$ and $\big<\mathbb{R},+\big>$ are not isomorphic. [They have the same cardinality]. The equation $x\cdot x=\beta$ has a solution $x,~\forall \beta\in\mathbb{C}$, but $x\cdot x=-1$ has no solution in $\mathbb{R}$.
\end{exa}
\end{note}
\begin{defn}[Identity Element]
	Let $\big<A,\star\big>$ be a binary structure. An element $e\in A$ is said to be an \textit{identity element} for $\star$ if $e\star a=a=a\star e,~\forall a\in A$.
\end{defn}
\begin{thm}[Uniqueness of the Identity Element]
	A binary structure $\big<A,\star\big>$ has at most one identity element. That is, the identity element is unique.
	\begin{proof}
		Let $e$ and $e'$ be two identity elements of $\big<A,\star\big>$. Then $e'=e'\star e=e$.
	\end{proof}
\end{thm}
\begin{exa} 
	Considering Example \ref{bo}, the identity element of $A$ is $b$ and that of $A'$ is $\$$.
\end{exa}
\begin{defn}[Semigroup]
	Any set together with an associative binary operation, is called a \textit{semigroup}.
\end{defn}
\begin{defn}[Monoid]
	A semigroup with an identity element is called a \textit{monoid}.
\end{defn}

\begin{exa}
	\begin{enumerate}
		\item[(i)] $\big<\mathbb{Z}^+,+\big>$ is a commutative semigroup that is not a monoid. [Show!!!] 
		\item[(ii)] Nonnegative integers under addition do form a monoid. [Try to show, to see what is happening]
	\end{enumerate}
\end{exa}