\contentsline {chapter}{Notation}{i}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Sets}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Sets}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}New Sets from Old Sets}{2}{section.2.2}
\contentsline {chapter}{\numberline {3}Relations}{4}{chapter.3}
\contentsline {section}{\numberline {3.1}Functions and Relations}{4}{section.3.1}
\contentsline {section}{\numberline {3.2}New Functions from Old Functions}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Equivalence Relations, Congruences and Partitions}{8}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\unskip \nobreakspace {}Equivalence Relation.\nobreakspace {}}{8}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Congruence Relation}{9}{section.3.4}
\contentsline {subsection}{\numberline {3.4.6}\unskip \nobreakspace {}Partitions.\nobreakspace {}}{10}{subsection.3.4.6}
\contentsline {chapter}{\numberline {4}Binary Operations.}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}Binary Operation}{13}{section.4.1}
\contentsline {section}{\numberline {4.2}Tables}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Isomorphic Binary Structures}{14}{section.4.3}
\contentsline {chapter}{\numberline {5}Groups}{16}{chapter.5}
\contentsline {section}{\numberline {5.1}Elementary Properties of Groups}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Finite Groups and Group Tables (Cayley Tables)}{17}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}\unskip \nobreakspace {}Properties of a Table.\nobreakspace {}}{18}{subsection.5.2.1}
\contentsline {section}{\numberline {5.3}Subgroups}{18}{section.5.3}
\contentsline {section}{\numberline {5.4}Cyclic Subgroups}{20}{section.5.4}
\contentsline {section}{\numberline {5.5}Cyclic Groups}{21}{section.5.5}
\contentsline {section}{\numberline {5.6}Subgroups of Finite Cyclic Groups }{22}{section.5.6}
\contentsline {section}{\numberline {5.7}Direct Products and Finitely Generated Abelian Groups}{24}{section.5.7}
\contentsline {section}{\numberline {5.8}Cosets and Theorem of Lagrange}{25}{section.5.8}
\contentsline {section}{\numberline {5.9}The Theorem of Lagrange}{26}{section.5.9}
\contentsline {section}{\numberline {5.10}Homomorphism}{27}{section.5.10}
\contentsline {section}{\numberline {5.11}Properties of Homomorphisms}{29}{section.5.11}
\contentsline {section}{\numberline {5.12}Factor Groups}{30}{section.5.12}
\contentsline {section}{\numberline {5.13}The Fundamental Homomorphism Theorem}{31}{section.5.13}
\contentsline {section}{\numberline {5.14}Normal Subgroups and Inner Automorphisms}{32}{section.5.14}
\contentsline {chapter}{\numberline {6}Rings and Fields}{34}{chapter.6}
\contentsline {section}{\numberline {6.1}Rings}{34}{section.6.1}
\contentsline {section}{\numberline {6.2}Fields}{36}{section.6.2}
\contentsline {section}{\numberline {6.3}Integral Domains}{37}{section.6.3}
